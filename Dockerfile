FROM openjdk:8-jdk-alpine
#expose port 8080
EXPOSE 8080

#default command
CMD java -jar /apps/hazelcast-server-0.0.1-SNAPSHOT.war

#copy hello world to docker image
ADD ./target/hazelcast-server-0.0.1-SNAPSHOT.war /apps/hazelcast-server-0.0.1-SNAPSHOT.war