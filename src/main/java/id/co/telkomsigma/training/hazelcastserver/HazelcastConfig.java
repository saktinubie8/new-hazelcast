package id.co.telkomsigma.training.hazelcastserver;

import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class HazelcastConfig {

    @Bean
    public Config config(){

        return new ClasspathXmlConfig("app-hazelcast.xml");
    }
}
